extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected
var dados

func _ready():
	var f = File.new()
	
	print("cena06 inicializada")
	
	f.open("data", File.READ)
	dados = f.get_var()
	selected = dados["Partido"]

	option.add_item("Escolha uma Opção")
	option.add_separator()
	#option recebe as opções de itens
	if selected == "Jacobino":
		option.add_item("OPÇÃO A) Recusa-se a inviolabilidade de Luís XVI, devendo, assim, julgá-lo")
		option.add_item("OPÇÃO B) Nós devemos julgar o rei como um inimigo!")
	elif selected == "Girondino":
		option.add_item("OPÇÃO A) A missão da atual Convenção é a de mudar o governo, não a de julgar Luís XVI")
		option.add_item("OPÇÃO B) A República não tem interesse algum em condenar Luís XVI")
	elif selected == "Planície":
		option.add_item("OPÇÃO A) Luís XVI pode ser julgado, conforme dito pelo comitê legislativo")
		option.add_item("OPÇÃO B) Luís XVI é inviolável como rei, não como indivíduo. Portanto, deve ser julgado")
	
	f.close()

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var name = dados["Nome"]
	var f = File.new()

	f.open("data", File.READ)
	dados = f.get_var()
	f.close()
	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena07"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena07.tscn")
