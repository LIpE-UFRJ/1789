extends TextureRect

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	print("Cena Transição.")


func _on_btn_menu_pressed():
	print("Volta para o Menu.")
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	print("Mudar cena.")
	get_tree().change_scene("res://scenes/menu.tscn")
