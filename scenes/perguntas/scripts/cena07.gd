extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected
var dados

func _ready():
	var f = File.new()
	
	print("cena07 inicializada")
	
	f.open("data", File.READ)
	dados = f.get_var()
	selected = dados["Partido"]

	option.add_item("Escolha uma Opção")
	option.add_separator()
	#option recebe as opções de itens
	if selected == "Jacobino":
		option.add_item("OPÇÃO A) No atual estado dos acontecimentos, Luís XVI deve ser assassinado.")
		option.add_item("OPÇÃO B) Luís XVI deve ser mantido em prisão ou em exílio")
	elif selected == "Girondino":
		option.add_item("OPÇÃO A) No atual estado dos acontecimentos, Luís XVI deve ser assassinado.")
		option.add_item("OPÇÃO B) Acredita-se que a melhor punição à Luís XVI seja o exílio.")
	elif selected == "Planície":
		option.add_item("OPÇÃO A) No atual estado dos acontecimentos, Luís XVI deve ser assassinado.")
		option.add_item("OPÇÃO B) Acredita-se que a melhor punição à Luís XVI seja o exílio ou a prisão")
	
	f.close()

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var name = dados["Nome"]
	var f = File.new()

	f.open("data", File.READ)
	dados = f.get_var()
	f.close()
	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena07 - transição"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena07 - transição.tscn")
